%global goipath github.com/doronbehar/termbox-go
%global forgeurl https://github.com/doronbehar/termbox-go
%global commit 8c9470559e0500587c27a1f3cf852961111859c9
Version:        1.0.0

%gometa

%global common_description %{expand:
Termbox is a library for creating cross-platform text-based interfaces.}

%global golicenses LICENSE
%global godocs README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Pure Go termbox implementation
License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%check
%gochecks

%gopkgfiles
