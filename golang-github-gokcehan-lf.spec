%global goipath github.com/gokcehan/lf
%global forgeurl https://github.com/gokcehan/lf
%global commit 50dd374f77049c167952ca917148bd357f5731e8
Version:        1

%gometa

%global common_description %{expand:
lf (as in "list files") is a terminal file manager written in Go. It is heavily
inspired by ranger with some missing and extra features. Some of the missing
features are deliberately omitted since they are better handled by external
tools. See faq for more information and tutorial for a gentle introduction with
screencasts.}

%global golicenses LICENSE
%global godocs README.md lf.1

Name:           %{goname}
Release:        16%{?dist}
Summary:        Terminal file manager
License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep


%generate_buildrequires
%go_generate_buildrequires

%build
%gobuild -o %{gobuilddir}/bin/lf %{goipath}/

%install
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/

%check
%gocheck

%files
%license LICENSE
%doc README.md
%{_bindir}/*

