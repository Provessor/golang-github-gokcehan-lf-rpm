# RPM spec for [lf](https://github.com/gokcehan/lf)

`.spec` to build the latest relase of `lf`, a terminal file manager
written in Go; heavily inspired by ranger with a slight difference in
features -- some are better served by external tools.

See the [FAQ](https://github.com/gokcehan/lf/wiki/FAQ),
[docs](https://godoc.org/github.com/gokcehan/lf) or
[GitHub](https://github.com/gokcehan/lf) for more information.

# Additional specs
## `lf-git`
The latest git of `lf`, see above.

## [`times.v1`](https://gopkg.in/djherbis/times.v1)
The latest release of times, a platform-independent
way to get atime, mtime, ctime and btime for files.

For more details see the [gopkg](https://gopkg.in/djherbis/times.v1),
[source](https://github.com/djherbis/times) or [API
documentation](https://godoc.org/gopkg.in/djherbis/times.v1).

