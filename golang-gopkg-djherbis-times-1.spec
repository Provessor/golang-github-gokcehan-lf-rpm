%global goipath gopkg.in/djherbis/times.v1
%global forgeurl https://github.com/djherbis/times
Version:        1.2.0

%gometa

%global common_description %{expand:
Package times provides a platform-independent way to get atime, mtime, ctime and btime for files.}

%global golicenses LICENSE
%global godocs README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        #golang file times (atime, mtime, ctime, btime)
License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%check
%gochecks

%gopkgfiles
